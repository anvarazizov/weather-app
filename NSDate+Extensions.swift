//
//  NSDate+Extensions.swift
//  TSWeather
//
//  Created by Anvar Azizov on 15/01/18.
//  Copyright (c) 2015 Anvar Azizov. All rights reserved.
//

import Foundation

extension NSDate {
	func toString(let format:String) -> String? {
		var formatter:NSDateFormatter = NSDateFormatter()
		formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX")
		
		if let timezone = Utils.loadFromUserDefaults(key: "Timezone") as? String {
			formatter.timeZone = NSTimeZone(name: timezone)
		}
		
		formatter.dateFormat = format
		return formatter.stringFromDate(self)
	}
}