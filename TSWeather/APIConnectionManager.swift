//
//  APIConnectionManager.swift
//  TSWeather
//
//  Created by Anvar Azizov on 15/01/17.
//  Copyright (c) 2015 Anvar Azizov. All rights reserved.
//

import Foundation
import CoreLocation

typealias ServiceResponse = (NSDictionary!, NSError?) -> Void

class APIConnectionManager: AFHTTPRequestOperationManager {
	class var sharedManager: APIConnectionManager {
		
		struct Static {
			static let instance: APIConnectionManager = APIConnectionManager()
		}
		
		return Static.instance
	}
	
	private let serviceRequest = "http://weatheritapi.ds.trustsourcing.com/GetData?lon=%@&lat=%@"
	
	func fetchWeatherData(latitude: String, longitude: String, onCompletion: ServiceResponse) {
		
		let urlString: String = String(format:serviceRequest, longitude, latitude)
		
		AFHTTPRequestOperationManager().GET(urlString, parameters: nil, success: { (operation: AFHTTPRequestOperation!, responseObject: AnyObject!) -> Void in
				onCompletion(responseObject as NSDictionary, nil)
			},
			
			failure: { (operation: AFHTTPRequestOperation!, error: NSError!) -> Void in
				println(error.localizedDescription)
		})
	}
}