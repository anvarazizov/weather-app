//
//  CoreDataManager.swift
//  TSWeather
//
//  Created by Anvar Azizov on 15/01/17.
//  Copyright (c) 2015 Anvar Azizov. All rights reserved.
//

import Foundation
import CoreData


class CoreDataManager: NSObject {
	
	var _managedObjectContext: NSManagedObjectContext? = nil
	var _managedObjectModel: NSManagedObjectModel? = nil
	var _persistentStoreCoordinator: NSPersistentStoreCoordinator? = nil
	
	class var shared:CoreDataManager{
		get {
			struct Static {
				static var instance : CoreDataManager? = nil
				static var token : dispatch_once_t = 0
			}
			dispatch_once(&Static.token) { Static.instance = CoreDataManager() }
			
			return Static.instance!
		}
	}
	
	// MARK: - Core Data stack
	
	lazy var applicationDocumentsDirectory: NSURL = {
		let urls = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)
		return urls[urls.count-1] as NSURL
		}()
	
	lazy var managedObjectModel: NSManagedObjectModel = {
		let modelURL = NSBundle.mainBundle().URLForResource("TSWeather", withExtension: "momd")!
		return NSManagedObjectModel(contentsOfURL: modelURL)!
		}()
	
	lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator? = {
		// Create the coordinator and store
		var coordinator: NSPersistentStoreCoordinator? = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
		let url = self.applicationDocumentsDirectory.URLByAppendingPathComponent("TSWeather.sqlite")
		var error: NSError? = nil
		var failureReason = "There was an error creating or loading the application's saved data."
		if coordinator!.addPersistentStoreWithType(NSSQLiteStoreType, configuration: nil, URL: url, options: nil, error: &error) == nil {
			coordinator = nil
			// Report any error we got.
			let dict = NSMutableDictionary()
			dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data"
			dict[NSLocalizedFailureReasonErrorKey] = failureReason
			dict[NSUnderlyingErrorKey] = error
			error = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
			
			NSLog("Unresolved error \(error), \(error!.userInfo)")
			abort()
		}
		
		return coordinator
		}()
	
	lazy var managedObjectContext: NSManagedObjectContext? = {
		let coordinator = self.persistentStoreCoordinator
		if coordinator == nil {
			return nil
		}
		var managedObjectContext = NSManagedObjectContext()
		managedObjectContext.persistentStoreCoordinator = coordinator
		return managedObjectContext
		}()
	
	
	// MARK: - Fetch requests
	
	func fetchWeatherPoints() -> NSArray {
		var results: NSArray
		var fetchError:NSError?
		
		let request = NSFetchRequest(entityName: "WeatherPoint");
		request.returnsObjectsAsFaults = true
		
		let sortDescriptor = NSSortDescriptor(key: "time", ascending: true)
		let sortDescriptors = [sortDescriptor]
		request.sortDescriptors = sortDescriptors
		

		let predicate = NSPredicate(format: "time >= %@", NSDate())
		request.predicate = predicate
		
		results = self.managedObjectContext!.executeFetchRequest(request, error: &fetchError)!
		
		if results.count == 0 {
			
		}
		
		if let error = fetchError {
			println("Warning!! \(error.description)")
		}
		
		return results
	}
	
	func cleanData() {
		var results: NSArray
		var fetchError:NSError?
		
		let request = NSFetchRequest(entityName: "WeatherPoint");
		request.returnsObjectsAsFaults = true
		
		results = self.managedObjectContext!.executeFetchRequest(request, error: &fetchError)!
		
		if results.count == 0 {
			println("Database already clean")
		}
		else
		{
			for object in results {
				self.managedObjectContext!.deleteObject(object as NSManagedObject)
			}
			self.saveContext()
		}
		
		if let error = fetchError {
			println("Warning!! \(error.description)")
		}
	}

	
	func executeFetchRequest(request:NSFetchRequest)-> Array<AnyObject>? {
		var results:Array<AnyObject>?
		var fetchError:NSError?
		results = self.managedObjectContext!.executeFetchRequest(request, error: &fetchError)
		
		if let error = fetchError {
			println("Warning!! \(error.description)")
		}
		
		return results
	}
	
	// MARK: - Save methods
	
	func saveContext () {
		if let moc = self.managedObjectContext {
			var error: NSError? = nil
			if moc.hasChanges && !moc.save(&error) {
				NSLog("Unresolved error \(error), \(error!.userInfo)")
				abort()
			}
		}
	}
}


