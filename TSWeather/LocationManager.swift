//
//  LocationManager.swift
//  TSWeather
//
//  Created by Anvar Azizov on 15/01/17.
//  Copyright (c) 2015 Anvar Azizov. All rights reserved.
//

import Foundation
import CoreLocation

class LocationManager: CLLocationManager {
	class var sharedManager: LocationManager {
		
		struct Static {
			static let instance: LocationManager = LocationManager()
		}
		
		return Static.instance
	}
	
	func updateLocation() {
		triggerLocationServices()
	}
	
	func triggerLocationServices() {
		if CLLocationManager.locationServicesEnabled() {
			
			if self.respondsToSelector("requestWhenInUseAuthorization") {
				self.requestWhenInUseAuthorization()
			}
			
			self.startUpdatingLocation()
		}
	}
}
