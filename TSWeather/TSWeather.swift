//
//  TSWeather.swift
//  TSWeather
//
//  Created by Anvar Azizov on 15/01/17.
//  Copyright (c) 2015 Anvar Azizov. All rights reserved.
//

import Foundation
import CoreData

class WeatherPoint: NSManagedObject {

    @NSManaged var time: NSDate
    @NSManaged var weatherDescription: String
    @NSManaged var imageName: String
    @NSManaged var tempMin: NSNumber
    @NSManaged var tempMax: NSNumber
    @NSManaged var srt: NSDate
    @NSManaged var sst: NSDate

}
