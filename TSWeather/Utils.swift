//
//  Utils.swift
//  TSWeather
//
//  Created by Anvar Azizov on 15/01/18.
//  Copyright (c) 2015 Anvar Azizov. All rights reserved.
//

import Foundation

class Utils {
	class func fahrenheitToCelcius(#fahrenheit: Double) -> (Int){
		let celsius: Double = (fahrenheit - 32) / 1.8
		return Int(round(celsius))
	}
	
	class func saveToUserDefaults(value: AnyObject?, forKey defaultName: String) {
		let defaults = NSUserDefaults.standardUserDefaults()
		defaults.setObject(value, forKey: defaultName)
		defaults.synchronize()
	}
	
	class func loadFromUserDefaults(key defaultName: String) -> AnyObject? {
		let defaults = NSUserDefaults.standardUserDefaults()
		if let object: AnyObject = defaults.objectForKey(defaultName) {
			return object
		}
		else
		{
			return nil
		}
	}
}