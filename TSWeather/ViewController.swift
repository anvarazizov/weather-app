//
//  ViewController.swift
//  TSWeather
//
//  Created by Anvar Azizov on 15/01/17.
//  Copyright (c) 2015 Anvar Azizov. All rights reserved.
//

import UIKit
import CoreLocation
import CoreData

class ViewController: UIViewController, CLLocationManagerDelegate, UITableViewDataSource {

	@IBOutlet weak var cityLabel: UILabel!
	@IBOutlet weak var detailsLabel: UILabel!
	@IBOutlet weak var degreesLabel: UILabel!
	@IBOutlet weak var otherDays: UITableView!
	@IBOutlet weak var sunriseTimeLabel: UILabel!
	@IBOutlet weak var sunsetTimeLabel: UILabel!
	
	var weatherItems = [WeatherPoint]()
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "deviceRotated", name: UIDeviceOrientationDidChangeNotification, object: nil)
        
		LocationManager.sharedManager.delegate = self
		LocationManager.sharedManager.updateLocation()
		
		otherDays.dataSource = self
		
		let weatherCellNib = UINib(nibName: "WeatherCell", bundle: nil)
		otherDays.registerNib(weatherCellNib, forCellReuseIdentifier: "WeatherCell")
		
		self.updateUI()
	}
	
	// MARK: UI
	
	private func updateUI() {
		weatherItems = CoreDataManager.shared.fetchWeatherPoints() as [WeatherPoint]
		
		if weatherItems.count > 0
		{
			for point in weatherItems {
				println(point.time)
			}
			
			let weatherItem = weatherItems[0]
			cityLabel.text = Utils.loadFromUserDefaults(key: "City") as? String
			detailsLabel.text = weatherItem.weatherDescription
			let degrees: Int = Utils.fahrenheitToCelcius(fahrenheit: weatherItem.tempMin.doubleValue)
			degreesLabel.text = NSString(format: "%d°", degrees)
			sunriseTimeLabel.text = NSString(format: "%@", weatherItem.srt.toString("HH:MM")!)
			sunsetTimeLabel.text = NSString(format: "%@", weatherItem.sst.toString("HH:MM")!)
			
			otherDays.reloadData()
		}
	}
	
	// MARK: - CLLocationManagerDelegate
	
	func locationManager(manager: CLLocationManager!, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
		if status == .AuthorizedWhenInUse || status == .Authorized {
			manager.startUpdatingLocation()
		}
	}
	
	func locationManager(manager: CLLocationManager!, didUpdateLocations locations: [AnyObject]!) {
		let location = locations.last as CLLocation
		let eventDate = location.timestamp
		let howRecent = eventDate.timeIntervalSinceNow
		
		if (abs(howRecent) < 60 * 60)
		{
			CLGeocoder().reverseGeocodeLocation(location, completionHandler: { (placemarks, error) -> Void in
				println(location)
				
				if error != nil {
					println("Reverse geocoder failed with error" + error.localizedDescription)
					return
				}
				
				if placemarks.count > 0 {
					let pm = placemarks[0] as CLPlacemark
					
					let lat = String(format: "%f", location.coordinate.latitude)
					let lon = String(format: "%f", location.coordinate.longitude)
					let locality = pm.locality
					
					
					println(locality, lat, lon)
					self.didFindLocation(locality, latitude: lat, longitude: lon)
				}
				else {
					println("Problem with the data received from geocoder")
				}
			})
			
			LocationManager.sharedManager.stopUpdatingLocation()
		}
	}
	
	func locationManager(manager: CLLocationManager!, didFailWithError error: NSError!) {
		
		updateDataForLatitude("", longitude: "")
		
		LocationManager.sharedManager.stopUpdatingLocation()
		println(error.localizedDescription)
	}
	
	private func didFindLocation(locality: String, latitude: String, longitude: String) {
		Utils.saveToUserDefaults(locality, forKey: "City")
		
		updateDataForLatitude(latitude, longitude: longitude)
	}
	
	// MARK: Weather data
	
	private func updateDataForLatitude(latitude: String, longitude: String) {
		let APImanager = APIConnectionManager.sharedManager
		
		APImanager.reachabilityManager.setReachabilityStatusChangeBlock { (status: AFNetworkReachabilityStatus) -> Void in
			switch status {
			case AFNetworkReachabilityStatus.ReachableViaWiFi, AFNetworkReachabilityStatus.ReachableViaWWAN:
				println("reachable")
				if (latitude != "" && longitude != "") {
					self.getWeatherWithLatitude(latitude: latitude, longitude: longitude)
				}
				
			case AFNetworkReachabilityStatus.NotReachable:
				println("not reachable")
				
				let alert = UIAlertView(title: "You're offline", message: "Please, connect to the internet to get weather", delegate: nil, cancelButtonTitle: "Cancel", otherButtonTitles: "Ok")
				alert.show()
				
				self.updateUI()
				
			default:
				println("unknown behaviour")
			}
		}
		
		APImanager.reachabilityManager.startMonitoring()
	}
	
	private func getWeatherWithLatitude(#latitude: String, longitude: String) {
		
		APIConnectionManager.sharedManager.fetchWeatherData(latitude, longitude: longitude, onCompletion: { (response: NSDictionary!, error: NSError?) -> Void in
			if error != nil {
				println(error?.localizedDescription)
			}
			else {
				println(response)
				
				let results = response.objectForKey("D") as NSArray
				let timeZoneName = response.objectForKey("Tz") as String
				
				Utils.saveToUserDefaults(timeZoneName, forKey: "Timezone")
				
				let managedObjectContext = CoreDataManager.shared.managedObjectContext as NSManagedObjectContext?
				
				CoreDataManager.shared.cleanData()
				
				for object in results {
					
					var weatherPoint = NSEntityDescription.insertNewObjectForEntityForName("WeatherPoint", inManagedObjectContext: managedObjectContext!) as WeatherPoint
					weatherPoint.weatherDescription = object["S"] as String
					weatherPoint.imageName = object["I"] as String
					weatherPoint.time = self.dateFromTimeInterval(object["T"] as NSTimeInterval)
					weatherPoint.tempMin = object["TempMin"] as Double
					weatherPoint.tempMax = object["TempMax"] as Double
					weatherPoint.srt = self.dateFromTimeInterval(object["SrT"] as NSTimeInterval)
					weatherPoint.sst = self.dateFromTimeInterval(object["SsT"] as NSTimeInterval)
				}
				
				CoreDataManager.shared.saveContext()
				
				self.updateUI()
			}
		})
	}
	
	// MARK: - TableView DataSource
	
	func numberOfSectionsInTableView(tableView: UITableView) -> Int {
		return 1
	}
	
	func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		if weatherItems.count >= 1 {
			return weatherItems.count - 1
		}
		else
		{
			return 0
		}
	}
	
	func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
		let cellIdentifier = "WeatherCell"
		
		let cell: WeatherCell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath) as WeatherCell
		
		let currentItem = weatherItems[indexPath.row + 1]
		
		let minDegrees: Int = Utils.fahrenheitToCelcius(fahrenheit: currentItem.tempMin.doubleValue)
		let maxDegrees: Int = Utils.fahrenheitToCelcius(fahrenheit: currentItem.tempMax.doubleValue)
		
		cell.weatherImage.image = UIImage(named: currentItem.imageName)
		cell.weatherDescriptionLabel.text = currentItem.weatherDescription
		cell.dateLabel.text = NSString(format: "%@", currentItem.time.toString("MMM, DD")!)
		cell.minTempLabel.text = NSString(format: "Min: %d°", minDegrees)
		cell.maxTempLabel.text = NSString(format: "Max: %d°", maxDegrees)
		cell.sunriseLabel.text = NSString(format: "Sunrise: %@", currentItem.srt.toString("HH:MM")!)
		cell.sunsetLabel.text = NSString(format: "Sunset: %@", currentItem.sst.toString("HH:MM")!)

		return cell
	}
	
	// MARK: Helpers
	
	private func dateFromTimeInterval(timeInterval: NSTimeInterval) -> NSDate {
		return NSDate(timeIntervalSince1970: timeInterval)
	}
    
    // MARK: Rotation
    
    func deviceRotated()
    {
        otherDays.reloadData()
    }
}

