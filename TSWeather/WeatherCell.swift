//
//  WeatherCell.swift
//  TSWeather
//
//  Created by Anvar Azizov on 15/01/18.
//  Copyright (c) 2015 Anvar Azizov. All rights reserved.
//

import Foundation

class WeatherCell: UITableViewCell {

	@IBOutlet weak var weatherImage: UIImageView!
	@IBOutlet weak var dateLabel: UILabel!
	@IBOutlet weak var weatherDescriptionLabel: UILabel!
	@IBOutlet weak var minTempLabel: UILabel!
	@IBOutlet weak var maxTempLabel: UILabel!
	@IBOutlet weak var sunriseLabel: UILabel!
	@IBOutlet weak var sunsetLabel: UILabel!
}
